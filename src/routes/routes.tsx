import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { NavBar } from '../components/NavBar';
import { Catalog } from '../pages/Catalog';
import { Home } from '../pages/Home';

export const MyRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/catalog" element={<Catalog />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};
