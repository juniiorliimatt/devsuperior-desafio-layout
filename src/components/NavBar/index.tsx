import './styles.css';
import 'bootstrap/js/src/collapse.js';
import { Link, NavLink } from 'react-router-dom';

export const NavBar = () => {
  return (
    <nav className=" container-fluid navbar navbar-expand-md navbar-light bg-primary main-nav">
      <div className="container-fluid">
        <Link to={'/'}>
          <h3>Carros Top</h3>
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#carrostop-navbar"
          aria-controls="carrostop-navbar"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="main-menu">
          <div className="collapse navbar-collapse" id="carrostop-navbar">
            <div className="menu-itens">
              <ul>
                <li>
                  <NavLink to={'/'}>Home</NavLink>
                </li>
                <li>
                  <NavLink to={'/catalog'}>Catálogo</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};
