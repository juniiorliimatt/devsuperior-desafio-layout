import './styles.css';
import { ReactComponent as MainImage } from '../../assets/img/main-image.svg';
import { Link } from 'react-router-dom';

export const Home = () => {
  return (
    <>
      <div className="home-container">
        <div className="home-card">
          <div className="home-content-container">
            <div className="home-image-container">
              <MainImage />
            </div>
            <h1>O carro perfeito para você</h1>
            <p>
              Conheça nossos carros e dê mais um passo na realização do seu
              sonho
            </p>
          </div>
          <div>
            <div className="card-container">
              <div className="text-button">
                <Link to={'/catalog'}>VER CATÁLOGO</Link>
              </div>
              <div className="text">
                <p>Comece agora a navegar</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
